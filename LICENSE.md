# License for the repo QCourse511-1

https://gitlab.com/qworld/qeducation/qcourse511-1


## TEXT

The text and figures are licensed under the Creative Commons Attribution 4.0 International Public License (CC-BY-4.0), available at https://creativecommons.org/licenses/by/4.0/legalcode. 

## CODE

Any code snippets in this repo are licensed under Apache License 2.0, available at http://www.apache.org/licenses/LICENSE-2.0.
